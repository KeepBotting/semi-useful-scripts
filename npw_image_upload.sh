#!/bin/bash

# This script accepts a path to a file, uploads that file to the
# netherworld server, and places a link to the file on the clipboard.

# Use like $ npw_image_upload.sh image.png

# Filenames will be overwritten (auto-generated from MD5 of image)
# to prevent collisions.

base_filename=$(basename "$1")
hashed_filename=$(echo $RANDOM | sha256sum | base64 | head -c 7)
file_extension="${base_filename##*.}"

final_file=$hashed_filename.$file_extension

scp -p "$1" branon@netherworld.pw:~/wwwshare/$final_file

echo "http://netherworld.pw:81/files/$final_file" | xclip

