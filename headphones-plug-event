#!/bin/bash

# Small script that polls ALSA to determine whether headphones are
# plugged or unplugged. Takes predetermined actions based on state
# of said headphones.

# Check headphones status every X seconds
POLL_RATE=1

# Default dB gain, -36.00 is 25% on my system
# Will be overwritten by the script, but avoids
# problems if the script is started while
# headphones are unplugged.
MASTER_DB=-36.00dB

# Node ID that corresponds to your headphones in /proc/asound/card0/codec#0
NODE_ID=0x10

# ID of your sound card
# For most people this will be 0
SOUND_CARD=0

# Whatever ALSA calls your speakers
# For most people this will be Speaker
DEVICE=Speaker

while [ 1 ]; do
  AMP_OUT_VAL=$(grep -A 8 "Node $NODE_ID" /proc/asound/card0/codec#0 | grep -c "Amp-Out vals:  \[0x80 0x80\]")

  if [ $AMP_OUT_VAL -eq 1 ]; then
    # [Plugged in] System already: mutes speaker, lowers master
    #              System doesn't: (nothing)
    #              We will:        store master dB gain
    MASTER_DB=$(amixer -c $SOUND_CARD -- get Master | grep dB | cut -d [ -f 3 | sed s/\]//)
    echo "Headphones are plugged in. System handles this correctly. Updating Master dB gain: $MASTER_DB"
  else
    # [Plugged out] System already: unmutes speaker, raises master
    #               System doesn't: raise speaker volume above 0
    #               We will:        raise speaker volume to 100%, normalize master, update master dB gain
    echo "Headphones are not plugged in. System handles this incorrectly. Normalizing Master, raising Speaker."
    # Prevent the script from interfering with a hypothetical user adjusting the volume
    MASTER_DB=$(amixer -c $SOUND_CARD -- get Master | grep dB | cut -d [ -f 3 | sed s/\]//)
    amixer -c $SOUND_CARD -- sset Master playback $MASTER_DB > /dev/null # Normalize master
    amixer -c $SOUND_CARD -- sset $DEVICE playback 100% > /dev/null      # Raise speaker
  fi

  sleep $POLL_RATE
done
