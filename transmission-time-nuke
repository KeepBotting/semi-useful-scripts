#!/bin/bash

# This script will automatically remove & delete torrents after a pre-defined period of time.
#
# It is loosely based on teambritta's "Auto-remove after X time script"
# Which can be found here: https://forum.transmissionbt.com/viewtopic.php?t=13427
#
# Currently, this script DOES NOT support instances of Transmission that lack an authentication
# requirement. So, if you're adamantly against adding an abritrary username & password, then
# I'm afraid that this script is not going to work for you, unless you manually modify all of
# the calls to transmission-remote and remove the authentication flag (-n). In the future, I
# will add an option to use the script whilst unauthenticated.


# If, on the other hand, authentication IS required to access Transmission, the process
# is as follows:
# 1. CFG_AUTH_USER should be equal to the username.
# 2. CFG_AUTH_PASSWD should path to a properly-permissioned file containing the password.


# The process for item 1 should be self-explanatory.
# To satisfy item 2, the process is as follows:
#
# 1. Run the following series of commands.
# This will create a file (including permissions) containing your Transmission password.
# The permissions are set as such for safety reasons. Maybe someday I'll implement a proper
# password-protection scheme.
#
# $ cd /opt/transmission-time-nuke (but feel free to place it anywhere)
# $ echo "your_transmission_password" | sudo tee transmission-time-nuke.passwd (but feel free to name it anything)
# $ sudo chmod u+r transmission-time-nuke.passwd
# $ sudo chmod go-rwx transmission-time-nuke.passwd
#
# 2. Configure ownership.
# The file must be owned by the user who will be executing this script, which in
# most cases should be the root user.
#
# If you invoked the `tee` command when creating your file (as per the above instructions),
# root already owns it and you may skip this step. Otherwise:
#
# $ sudo chown root:root transmission-time-nuke.passwd
#
# 3. Ensure that CFG_AUTH_PASSWD (located in the config options below) paths to this file.

# 4. Optional step: execute the script in preview mode (--preview or -p) to ensure that it's
# not going to screw anything up when it runs for real. Preview mode logs to the terminal
# rather than a file, for easy debugging, and will not touch any of your torrents.

# Logs are saved to /opt/transmision-time-nuke/logs/YYYY-MM-DD-HHMM.log

# Put this script in root's crontab for best effect.

##############################################
###         Configuration options.         ###
##############################################

# Path to the directory in which completed torrents are stored
CFG_TARGET_DIRECTORY="/media/Torrents"

# Username used for authenticating with Transmission
CFG_AUTH_USER=branon

# Path to the file containing the password used for authenticating with Transmission
CFG_AUTH_PASSWD="/opt/transmission-time-nuke/transmission-time-nuke.passwd"

# The path to transmission-remote's executable
# Most commonly "transmission-remote" or "/usr/bin/transmission-remote"
CFG_EXECUTABLE="/usr/bin/transmission-remote"

# Torrents must be >= this number of days old, before they are deleted.
CFG_DELETION_THRESHOLD=14


################################################################
### Don't edit below here unless you know what you're doing. ###
################################################################


##############################
### Generate internal data ###
##############################

__VERSION="1.0"
__HOME_PATH="/opt/transmission-time-nuke"
__LOG_PATH="$__HOME_PATH/logs"
__LOG_FILE="$__LOG_PATH/`date +"%Y-%m-%d-%H%M"`.log"
__DELETION_THRESHOLD=`expr 86400 \* $CFG_DELETION_THRESHOLD`

if [ "$*" == "--preview" ] || [ "$*" == "-p" ]; then
  __PREVIEW_MODE=0
  __LOG_FILE="nowhere (preview mode)"
fi

##########################
### Verify directories ###
##########################

if [ ! -d "$__HOME_PATH" ]; then
  mkdir $__HOME_PATH;
fi

if [ ! -d "$__LOG_PATH" ]; then
  mkdir $__LOG_PATH;
fi

#####################
### Begin logging ###
#####################

if [ ! "$__PREVIEW_MODE" ]; then
  touch $__LOG_FILE
  exec 3>&1 1>>$__LOG_FILE 2>&1 # http://stackoverflow.com/questions/18460186/
fi

echo "transmission-time-nuke version 1.0        Copyright (c) 2015 Branon McClellan"
echo ""
echo "Running with arguments: $@"
echo ""
echo "Logging to: $__LOG_FILE"
echo "Running as Transmission user: $CFG_AUTH_USER"
echo "Deletion threshold is set at: $CFG_DELETION_THRESHOLD days ($__DELETION_THRESHOLD seconds)"
echo ""

########################
### Get the password ###
########################

if [ ! -f "$CFG_AUTH_PASSWD" ]; then
  echo "Cannot find password file. Ensure that you have created it correctly, and that \$CFG_AUTH_PASSWD paths to it. Exiting..."
  exit;
else
  __AUTH_PASSWD=`cat $CFG_AUTH_PASSWD`
  if [ -n "$__AUTH_PASSWD" ]; then
    echo "Received a password from $CFG_AUTH_PASSWD";
  else
    echo "Something went wrong, and a password could not be recieved. Exiting..."
    exit;
  fi
fi

echo ""

#######################
### Verify packages ###
#######################

function hasPackage { # http://stackoverflow.com/questions/1298066/
  echo -n "Looking for package: $1... "
  if [ $(dpkg-query -W -f='${Status}' $1 2>/dev/null | grep -c "ok installed") -ge 1 ]; then
    echo "[ OK ]"
    return 0;
  else
    echo "[ FAIL ]"
    echo "Missing package: $1, exiting..."
    return 1;
  fi
}

if ! hasPackage transmission-daemon; then exit; fi
if ! hasPackage transmission-cli; then exit; fi

echo ""

######################
### Verify daemons ###
######################

function hasProcess {
  echo -n "Looking for process: $1... "
  if [ $(ps -ef | grep -v grep | grep $1 | wc -l) -ge 1 ]; then
    echo "[ OK ]"
    return 0;
  else
    echo "[ FAIL ]"
    echo "Missing process: $1, exiting..."
    return 1;
  fi
}

if ! hasProcess transmission-daemon; then exit; fi

echo ""

#######################
### Verify password ###
#######################

echo "TODO: password verification."

# Tokenize over newlines instead of spaces.
OLDIFS=$IFS
IFS="
"
############################################
### Generate a list of finished torrents ###
############################################

# Contact the executable for a list of torrents
# grep the output to ensure only 100%-completed torrents are examined
for __TORRENT in `$CFG_EXECUTABLE -n $CFG_AUTH_USER:$__AUTH_PASSWD -l | grep 100%`; do
  echo ""
  # Extract the ID from grep's output with some sed magic
  __ID=`echo $__TORRENT | sed "s/^ *//g" | sed "s/ *100%.*//g"`

  # Determine the name of the item in question by contacting the executable again
  __NAME=`$CFG_EXECUTABLE -n $CFG_AUTH_USER:$__AUTH_PASSWD -t $__ID -f | head -1 | sed "s/ ([0-9]\+ files)://g"`
  echo "Examining torrent with ID: $__ID and name: $__NAME"

  # If ithe item is a directory, we'll base our determination off of the oldest file inside of it
  if [ -d "$CFG_TARGET_DIRECTORY/$__NAME" ]; then
    echo "Torrent is a directory"
    __OLDEST=0
    for __FILE in `find $CFG_TARGET_DIRECTORY/$__NAME`; # Loop through all the files
    do
      __AGE=`stat "$__FILE" -c%Y` # Determine the age of the file
      if [ $__AGE -gt $__OLDEST ]; then # If the age of this file is > the age of the previous file
        __OLDEST=$__AGE # Then this file is the oldest file in the directory
      fi
    done

  # If the item is not a directory, things are easier
  else
    echo "Torrent is a file"
    __OLDEST=`stat "$CFG_TARGET_DIRECTORY/$__NAME" -c%Y`
  fi

  __DIFFERENCE=$(expr $(date +%s) - $__OLDEST)
  echo "Torrent was added $(expr $__DIFFERENCE / 86400) days ($__DIFFERENCE seconds) ago"

  # Remove & delete the torrent if it's >= than the threshold
 # if (( __DIFFERENCE > __DELETION_THRESHOLD )); then
   if [ $__DIFFERENCE -ge $__DELETION_THRESHOLD ]; then
    echo "Torrent has been slated for deletion"
    if [ ! "$__PREVIEW_MODE" ]; then
      # Contact the executable and request that the torrent be removed, and its data deleted
      $CFG_EXECUTABLE -n $CFG_AUTH_USER:$__AUTH_PASSWD -t $__ID --remove-and-delete
    fi
  else
    echo "Torrent will not be deleted"
  fi

done

IFS=$OLDIFS
echo ""
